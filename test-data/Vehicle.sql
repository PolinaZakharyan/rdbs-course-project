INSERT INTO Vehicle (Id,RegistrNum,Brand,Launched,Commissioned,Decommissioned,Category,CarType,Capacity,Garage) VALUES
	 (1,'AH1500AA','Ruta 25',2006-01-01,'2009-01-01',NULL,'Passenger','Bus',19,1),
	 (2,'AH1456AA','Ruta 37',2009-01-01,'2009-01-01',NULL,'Passenger','Bus',23,1),
	 (3,'AH9859CM','MAZ 55160',2005-01-01,'2007-01-01',NULL,'Cargo','Truck',20,2),
	 (4,'HF4157EG','BAZ AO 79.24',2006-07-14,'2009-02-04',NULL,'Passenger','Bus',19,1),
	 (5,'AH3083EG','Ruta 25',2007-02-23,'2012-05-15',NULL,'Passenger','Bus',16,1),
	 (6,'EF6019DG','Ruta 30 ZNG',2006-02-15,'2016-11-13',NULL,'Passenger','Bus',15,1),
	 (7,'FH5967FC','BAZ AO 79.24',2005-01-24,'2016-06-06',NULL,'Passenger','Bus',17,1),
	 (8,'HD6306GC','Ruta 37',2005-11-23,'2010-10-15',NULL,'Passenger','Bus',15,1),
	 (9,'DG3217BB','GAZ 2217',2007-06-17,'2014-12-07',NULL,'Passenger','Bus',15,1),
	 (10,'AH4991GE','BAZ AO 79.24',2005-02-02,'2014-07-14',NULL,'Passenger','Bus',13,1);
INSERT INTO Vehicle (Id,RegistrNum,Brand,Launched,Commissioned,Decommissioned,Category,CarType,Capacity,Garage) VALUES
	 (11,'AF8581BG','GAZ 2217',2005-04-30,'2010-02-04',NULL,'Passenger','Bus',19,1),
	 (12,'DA7116FC','GAZ 2217',2007-08-21,'2012-03-28',NULL,'Passenger','Bus',17,1),
	 (13,'GA6297CB','Bogdan A09201',2007-03-27,'2010-02-22',NULL,'Passenger','Bus',17,1),
	 (14,'AA2977DD','Ruta 25',2005-04-26,'2012-01-19','2022-06-02','Passenger','Bus',14,1),
	 (15,'FG9388HG','Bogdan A09201',2005-02-14,'2015-01-01',NULL,'Passenger','Bus',19,1),
	 (16,'EE9168HB','Bogdan A09201',2006-06-13,'2012-05-07',NULL,'Passenger','Bus',13,1),
	 (17,'AE3560BA','Ruta 30 ZNG',2006-02-05,'2011-07-16',NULL,'Passenger','Bus',15,1),
	 (18,'HG8219HG','GAZ 2217',2005-06-29,'2017-03-11',NULL,'Passenger','Bus',16,1),
	 (19,'BE1615DC','Ruta 37',2007-10-22,'2011-11-21',NULL,'Passenger','Bus',14,1),
	 (20,'EH5710EA','Bogdan A09201',2007-02-09,'2009-06-04',NULL,'Passenger','Bus',15,1);
INSERT INTO Vehicle (Id,RegistrNum,Brand,Launched,Commissioned,Decommissioned,Category,CarType,Capacity,Garage) VALUES
	 (21,'HF5499DG','GAZ 2217',2008-09-04,'2013-08-04',NULL,'Passenger','Bus',16,1),
	 (22,'AF6233BH','Bogdan A09201',2007-01-11,'2018-09-11',NULL,'Passenger','Bus',13,1),
	 (23,'DA7419GF','MAZ 5516',2007-10-31,'2017-10-27',NULL,'Cargo','Truck',30,2),
	 (24,'AF6730DC','KRAZ 256B',2006-09-12,'2010-03-03',NULL,'Cargo','Truck',13,2),
	 (25,'AA8660CF','KRAZ 256B',2006-05-09,'2018-01-27','2019-04-03','Cargo','Truck',13,2),
	 (26,'AD8421EG','ZIL 4331',2005-03-01,'2014-08-19',NULL,'Cargo','Truck',8,2),
	 (27,'AE5675FC','KRAZ 6510',2008-10-07,'2010-07-16',NULL,'Cargo','Truck',14,2),
	 (28,'GB4027FC','MAZ 5516',2006-09-21,'2016-06-29','2020-10-11','Cargo','Truck',29,2),
	 (29,'BC3025HA','KRAZ 6510',2007-11-29,'2015-05-05',NULL,'Cargo','Truck',14,2),
	 (30,'FB5665CA','MAZ 5551',2006-09-28,'2018-10-06','2023-04-12','Cargo','Truck',22,2);
INSERT INTO Vehicle (Id,RegistrNum,Brand,Launched,Commissioned,Decommissioned,Category,CarType,Capacity,Garage) VALUES
	 (31,'DG6043GD','MAZ 5551',2008-07-08,'2018-03-23',NULL,'Cargo','Truck',26,2),
	 (32,'GD2528GB','ZIL 4331',2007-05-31,'2009-09-05',NULL,'Cargo','Truck',24,2),
	 (33,'FD3060GB','Peugeot 301',2005-07-03,'2010-08-16',NULL,'Passenger','Taxi',4,3),
	 (34,'BD3681AF','Peugeot 301',2008-09-23,'2011-10-11',NULL,'Passenger','Taxi',6,3),
	 (35,'AB1734GC','Renault Logan',2005-02-05,'2016-02-17',NULL,'Passenger','Taxi',6,3),
	 (36,'BG3985DD','Honda Accord',2008-10-01,'2011-07-16',NULL,'Passenger','Taxi',6,3),
	 (37,'CE5808DC','Toyota Corolla',2005-01-13,'2016-10-26',NULL,'Passenger','Taxi',4,3),
	 (38,'FF1238FF','Toyota Corolla',2005-04-28,'2017-06-21',NULL,'Passenger','Taxi',4,3),
	 (39,'AE6735DG','Hyundai Accent',2007-12-25,'2010-01-11',NULL,'Passenger','Taxi',4,3),
	 (40,'HA1771FC','Honda Accord',2005-08-24,'2009-12-10',NULL,'Passenger','Taxi',5,3);
INSERT INTO Vehicle (Id,RegistrNum,Brand,Launched,Commissioned,Decommissioned,Category,CarType,Capacity,Garage) VALUES
	 (41,'HD7929GF','Peugeot 301',2007-06-21,'2013-04-14',NULL,'Passenger','Taxi',4,3),
	 (42,'GE1384CH','Honda Accord',2007-02-27,'2014-06-04',NULL,'Passenger','Taxi',6,3),
	 (43,'GH7976DB','Scoda Rapid',2007-08-29,'2011-12-26',NULL,'Passenger','Taxi',5,3);
