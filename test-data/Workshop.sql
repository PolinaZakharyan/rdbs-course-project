INSERT INTO Workshop (Id,Name) VALUES
	 (1,'Administration'),
	 (2,'Aggregate'),
	 (3,'Locksmith'),
	 (4,'Electrical'),
	 (5,'Power system devices'),
	 (6,'Battery'),
	 (7,'Vulcanizing'),
	 (8,'Forging&Spring'),
	 (9,'Copper'),
	 (10,'Welding');

INSERT INTO Workshop (Id,Name) VALUES
	 (11,'Tin'),
	 (12,'Reinforcing'),
	 (13,'Upholster'),
	 (14, 'Woodworking'),
	 (15,'Tire'),
	 (16,'Fuel'),
	 (17,'Paint'),
	 (18,'Car wash'),
	 (19,'Dispatcher'),
	 (20, 'Driver');
