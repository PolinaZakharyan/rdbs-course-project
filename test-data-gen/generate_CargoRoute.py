from datetime import date, timedelta
import os
import sqlite3
from sqlite3 import Connection, Cursor

from random import randint, choice, choices

from tabulate import tabulate

RES_DIR = os.path.dirname(__file__)

CONNECTION = sqlite3.connect(os.path.join(RES_DIR, 'mydata.db'))
CURSOR = CONNECTION.cursor()

def Dest():
    routes = (
    ("Obukhiv - Zaporizhzhya", 896),
    ("Obukhiv - Vinnytsya", 398),
    ("Obukhiv - Uzhhorod", 1254),
    ("Obukhiv - Ternopil", 730),
    ("Obukhiv - Mykolayiv", 804),
    ("Obukhiv - L'viv", 936),
    ("Obukhiv - Luts'k", 734),
    ("Obukhiv - Khmel'nyts'kyy", 554),
    ("Obukhiv - Ivano-Frankivs'k", 902),
    ("Obukhiv - Kropyvnyts'kyi", 500),
    ("Obukhiv - Kherson", 902),
    ("Obukhiv - Kharkiv", 818),
    ("Obukhiv - Dnipropetrovsk", 788),
    ("Obukhiv - Chernivtsi", 820),
    ("Obukhiv - Chernihiv", 256),
    ("Obukhiv - Cherkasy", 314),
    ("Obukhiv - Zhytomyr", 268),
    ("Obukhiv - Zakarpattia", 1256),
    ("Obukhiv - Volyn", 736),
    ("Obukhiv - Vinnyts'ka", 400),
    ("Obukhiv - Sumy", 610),
    ("Obukhiv - Rivne", 604),
    ("Obukhiv - Poltava", 606),
    ("Obukhiv - Odessa", 882),
    ("City Center - Train Station", 6),
    ("City Hospital - Green Forest Residential Area", 10),
    ("Park Stop - Mega Mall", 6),
    ("Obukhiv-2 Airport - Premier Hotel", 20),
    ("Sunny City Residential Complex - Regional Hospital", 14),
    ("Train Station - New City Residential Area", 10),
    ("Central Park - Historical Museum", 4),
    ("Bus Terminal - Zvezda Cinema", 4),
    ("Podgorne Residential Area - Athlet Sports Arena", 14),
    ("University - River Port", 18)
)
    return choice(routes)

cargo1 = (
    'flour', 'soot', 'lime', 'cement', 'fertilizers', 'sand', 'crushed stone', 'peat', 'coal', 'grain', 
    'sugar', 'stone', 'roundwood','firewood', 'soil', 'ore', 'salt', 'meal', 'potatoes', 'beets'
)

cargo2 = ('passenger')

DATE_ALPHA = date(2022,9,1)
DATE_OMEGA = date.today()

def GenerateDates(fr: date, to: date):
    route = fr

    while route <= to:
        yield route
        route += timedelta(days=1)

dates = GenerateDates(DATE_ALPHA, DATE_OMEGA)



IdVD = [(3, 99), (3, 139), (33, 140), (23, 179), (23, 180), (24, 181), (24, 182), (26, 183), 
(26, 184), (27, 185), (27, 186), (29, 187), (29, 188), (31, 189), (31, 190), (32, 191), 
(32, 192), (34, 193), (34, 194), (35, 195), (35, 196), (36, 197), (36, 198), (37, 199), 
(37, 200), (38, 201), (38, 202), (39, 203), (39, 204), (40, 205), (40, 206), (41, 207), 
(41, 208), (42, 209), (42, 210), (43, 211), (43, 212)]

def RouteDate():
    pass



def VecDrCar():
    IdVD = [(3, 99), (3, 139), (33, 140), (23, 179), (23, 180), (24, 181), (24, 182), (26, 183), 
    (26, 184), (27, 185), (27, 186), (29, 187), (29, 188), (31, 189), (31, 190), (32, 191), 
    (32, 192), (34, 193), (34, 194), (35, 195), (35, 196), (36, 197), (36, 198), (37, 199), 
    (37, 200), (38, 201), (38, 202), (39, 203), (39, 204), (40, 205), (40, 206), (41, 207), 
    (41, 208), (42, 209), (42, 210), (43, 211), (43, 212)]
    IdVecDr = choice(IdVD)
    if IdVecDr[0] >= 32:
        carg = cargo2
    else:
        carg = choice(cargo1)
    return *IdVecDr, carg


id = 0
data = []
try:
    while True:
        for date in GenerateDates(DATE_ALPHA, DATE_OMEGA):
            for _ in range(randint(10, 20)):
                id += 1
                data.append((id, date, *VecDrCar(), *Dest()))
                if id >= 30000:
                    raise
except: pass





#id, RouteDate,    IdVehicle,  IdDriver,  CargoType,  Destination, Mileage,
#data =  [ (i,  RouteDate(), IdVehicle(1), IdDriver(), CargoType(), Destination, Mileage()) for i in range(4, 23)]

print(tabulate(data))

CURSOR.executemany("INSERT INTO CargoRoute VALUES(?,?,?,?,?,?,?)", data)
CONNECTION.commit()
