from datetime import date
import os
import sqlite3
from sqlite3 import Connection, Cursor

from random import randint, choice, choices

from tabulate import tabulate

RES_DIR = os.path.dirname(__file__)

CONNECTION = sqlite3.connect(os.path.join(RES_DIR, 'mydata.db'))
CURSOR = CONNECTION.cursor()


    # Id integer PRIMARY KEY,
    # RegistrNum varchar NOT NULL,
    # Brand varchar NOT NULL,
    # Launched integer,
    # Commissioned DATE,
    # Decommissioned DATE,
    # Category varchar,
    # CarType varchar,
    # Capacity integer,
    # Garage integer

    # Id  RegistrNum    Brand   Launched   Commissioned   Decommissioned Category CarType  Capacity  Garage
    # (6,   'AA354GH',    'Zapor',     'data',      'data', 'data',        'enum',  'enum',  int,      [1-3])

DATE_ALPHA = date(2005,1,1)
COMMISSIONED_BEGIN = date(2009,1,1)
DECOMMISSIONED_BEGIN = date(2019,1,1)
DATE_OMEGA = date.today()


def RandDate(fr: date, to: date):
    return date(
        year=randint(fr.year, to.year),
        month=randint(fr.month, to.month),
        day=randint(fr.day, to.day),
    )

def RegistrNum():
    pref = ''.join(choices('ABCDEFGH', k=2))
    num = str(randint(1000, 9999))
    suff = ''.join(choices('ABCDEFGH', k=2))
    return pref + num + suff

def Brand(garage):
    return choice({
        1: ['Ruta 30 ZNG', 'BAZ AO 79.24', 'Bogdan A09201', 'GAZ 2217'],
        2: ['KRAZ 6510', 'KRAZ 256B', 'MAZ 5516', 'MAZ 5551', 'ZIL 4331'],
        3: ['Toyota Corolla', 'Renault Logan', 'Hyundai Accent', 'Peugeot 301', 'Scoda Rapid', 'Honda Accord'],
    }[garage])

def Launched():
    return RandDate(DATE_ALPHA, COMMISSIONED_BEGIN)

def Commissioned():
    return RandDate(COMMISSIONED_BEGIN, DECOMMISSIONED_BEGIN)

def Decommissioned():
    return choices([RandDate(DECOMMISSIONED_BEGIN, DATE_OMEGA), None], weights=[.1, .9])[0]

#def Category():
#    return choice({
#       1: ['Cargo'],
#       2: ['Passenger'],
#    }[Category])

def CarType():
    return choice([
        'Bus',
        'Truck',
    ])
def Capacity(a, b):
    return randint(a, b)
def Garage():
    return randint(1,3)

data =  [ (i,  RegistrNum(), Brand(1), Launched(), Commissioned(), Decommissioned(), 'Passenger', 'Bus'  , Capacity(13, 20), 1) for i in range(4, 20)]
data += [ (i,  RegistrNum(), Brand(2), Launched(), Commissioned(), Decommissioned(), 'Cargo', 'Truck', Capacity(8, 30), 2) for i in range(20, 30)]
data += [ (i,  RegistrNum(), Brand(3), Launched(), Commissioned(), Decommissioned(), 'Passenger', 'Taxi' , Capacity(4, 6 ), 3) for i in range(30, 40)]


print(tabulate(data))

CURSOR.executemany("INSERT INTO Vehicle VALUES(?,?,?,?,?,?,?,?,?,?)", data)
CONNECTION.commit()