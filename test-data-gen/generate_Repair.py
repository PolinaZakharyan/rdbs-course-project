from datetime import date, timedelta
import os
import sqlite3
from sqlite3 import Connection, Cursor

from random import randint, choice

from tabulate import tabulate

RES_DIR = os.path.dirname(__file__)

CONNECTION = sqlite3.connect(os.path.join(RES_DIR, 'mydata.db'))
CURSOR = CONNECTION.cursor()

def TypeEmCost():
    RepairType = (
        ( "Alternator Replacement", choice([61, 101, 62, 102]), randint(250, 1000)),
        ( "Starter Motor Replacement", choice([61, 101, 62, 102]), randint(250, 750)),
        ( "Radiator Replacement", choice([61, 101, 62, 102]), randint(250, 750)),
        ( "Water Pump Replacement", choice([61, 101, 62, 102]), randint(500, 1500)),
        ( "Fuel Pump Replacement", choice([61, 101, 62, 102]), randint(500, 1500)),
        ( "Exhaust System Repair", choice([61, 101, 62, 102]), randint(500, 5000)),
        ( "Clutch Replacement", choice([63, 103, 64, 104]), randint(1500, 4000)),
        ( "Shock Absorber Replacement", choice([63, 103, 64, 104]), randint(750, 2000)),
        ( "Brake Pad Replacement", choice([63, 103, 64, 104]), randint(500, 1500)),
        ( "Brake Disc Replacement", choice([63, 103, 64, 104]), randint(1000, 2500)),
        ( "Suspension Repair", choice([63, 103, 64, 104]), randint(750, 2500)),
        ( "Electronics Diagnostic", choice([65, 105, 66, 106]), randint(250, 750)),
        ( "Wiring Repair and Replacement", choice([65, 105, 66, 106]), randint(500, 1500)),
        ( "Alternator Testing and Repair", choice([65, 105, 66, 106]), randint(500, 1500)),
        ( "Starter Motor Testing and Repair", choice([65, 105, 66, 106]), randint(500, 1500)),
        ( "Fuse and Relay Replacement", choice([65, 105, 66, 106]), randint(250, 500)),
        ( "Ignition System Repair", choice([65, 105, 66, 106]), randint(750, 2000)),
        ( "Lighting System Repair", choice([65, 105, 66, 106]), randint(250, 1000)),
        ( "Power Window and Door Lock Repair", choice([65, 105, 66, 106]), randint(500, 1500)),
        ( "Starter Motor Testing and Repair", choice([67, 107, 68, 108]), randint(500, 1500)),
        ( "Voltage Regulator Replacement", choice([67, 107, 68, 108]), randint(500, 1250)),
        ( "Charging System Diagnostics and Repair", choice([67, 107, 68, 108]), randint(500, 1500)),
        ( "Electrical System Voltage Testing", choice([67, 107, 68, 108]), randint(250, 500)),
        ( "Charging System Diagnostics and Repair", choice([67, 107, 68, 108]), randint(500, 1500)),
        ( "Power Inverter Installation and Repair", choice([67, 107, 68, 108]), randint(750, 2000)),
        ( "Generator Testing and Repair", choice([67, 107, 68, 108]), randint(500, 1500)),
        ( "Power Steering System Repair", choice([67, 107, 68, 108]), randint(750, 2000)),
        ( "Electric Fan and Cooling System Repair", choice([67, 107, 68, 108]), randint(500, 1250)),
        ( "Battery Testing and Diagnosis", choice([69, 109, 70, 110]), randint(250, 500)),
        ( "Battery Replacement", choice([69, 109, 70, 110]), randint(500, 750)),
        ( "Battery Terminal Cleaning and Maintenance", choice([69, 109, 70, 110]), randint(250, 500)),
        ( "Battery Cable Replacement", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Load Testing", choice([69, 109, 70, 110]), randint(250, 500)),
        ( "Battery Charging System Inspection", choice([69, 109, 70, 110]), randint(250, 500)),
        ( "Battery Voltage Testing and Calibration", choice([69, 109, 70, 110]), randint(250, 500)),
        ( "Battery Disposal and Recycling", choice([69, 109, 70, 110]), randint(25, 500)),
        ( "Tire Patching and Repair", choice([71, 111, 72, 112]), randint(250, 2000)),
        ( "Tire Balancing", choice([71, 111, 72, 112]), randint(250, 2000)),
        ( "Tire Rotation", choice([71, 111, 72, 112]), randint(250, 500)),
        ( "Tire Alignment Check", choice([71, 111, 72, 112]), randint(250, 2000)),
        ( "Spring Replacement", choice([73, 113, 74, 114]), randint(750, 2500)),
        ( "Suspension Spring Adjustment", choice([73, 113, 74, 114]), randint(750, 2000)),
        ( "Leaf Spring Repair", choice([73, 113, 74, 114]), randint(1000, 2500)),
        ( "Coil Spring Inspection and Maintenance",choice([73, 113, 74, 114]),  randint(500,1250)),
        ( "Spring Bushing Replacement", choice([73, 113, 74, 114]), randint(500, 1500)),
        ( "Electrical Wiring Repair", choice([75, 115, 76, 116]), randint(500, 1500)),
        ( "Wire Splicing and Soldering", choice([75, 115, 76, 116]), randint(500, 1500)),
        ( "Connector Replacement", choice([75, 115, 76, 116]), randint(500, 1500)),
        ( "Cable Harness Repair", choice([75, 115, 76, 116]), randint(750, 2000)),
        ( "Circuit Board Soldering", choice([75, 115, 76, 116]), randint(500, 1500)),
        ( "Body Panel Welding and Repair", choice([77, 117, 78, 118]), randint(500, 2000)),
        ( "Frame Straightening and Welding", choice([77, 117, 78, 118]), randint(1000, 2500)),
        ( "Exhaust System Welding and Repair", choice([77, 117, 78, 118]), randint(500, 1500)),
        ( "Chassis Welding and Reinforcement", choice([77, 117, 78, 118]), randint(750, 2000)),
        ( "Rust and Corrosion Repair", choice([77, 117, 78, 118]), randint(500, 1500)),
        ( "Dent Repair and Removal", choice([79, 119, 80, 120]), randint(500, 1500)),
        ( "Rust and Corrosion Repair", choice([79, 119, 80, 120]), randint(500, 1500)),
        ( "Scratch and Scuff Removal", choice([79, 119, 80, 120]), randint(250, 750)),
        ( "Panel Replacement and Welding", choice([79, 119, 80, 120]), randint(1000, 2500)),
        ( "Sheet Metal Fabrication", choice([79, 119, 80, 120]), randint(750, 2000)),
        ( "Bodywork Repair", choice([79, 119, 80, 120]), randint(500, 2500)),
        ( "Sheet metal tasks", choice([79, 119, 80, 120]), randint(500, 2500)),
        ( "Structural Frame Reinforcement", choice([81, 121, 82, 122]), randint(750, 2000)),
        ( "Chassis Strengthening", choice([81, 121, 82, 122]), randint(750, 2000)),
        ( "Roll Cage Installation", choice([81, 121, 82, 122]), randint(1000, 2500)),
        ( "Frame Welding and Repair", choice([81, 121, 82, 122]), randint(750, 2000)),
        ( "Structural Member Replacement", choice([81, 121, 82, 122]), randint(750, 2000)),
        ( "Seat Repair and Upholstery", choice([83, 123, 84, 124]), randint(750, 2000)),
        ( "Dashboard and Console Refurbishment", choice([83, 123, 84, 124]), randint(750, 1500)),
        ( "Carpet Replacement and Cleaning", choice([83, 123, 84, 124]), randint(500, 1000)),
        ( "Headliner Repair and Replacement", choice([83, 123, 84, 124]), randint(500, 1250)),
        ( "Interior Detailing and Restoration", choice([83, 123, 84, 124]), randint(500, 1500)),
        ( "Interior Wooden Trim Repair", choice([85, 125, 86, 126]), randint(500, 1500)),
        ( "Dashboard Wood Veneer Repair", choice([85, 125, 86, 126]), randint(750, 2000)),
        ( "Wooden Steering Wheel Refinishing", choice([85, 125, 86, 126]), randint(750, 2000)),
        ( "Wooden Panel Replacement", choice([85, 125, 86, 126]), randint(1000, 2500)),
        ( "Wooden Gear Shift Knob Restoration", choice([85, 125, 86, 126]), randint(500, 1250)),
        ( "Tire Replacement", choice([87, 127, 88, 128]), randint(250, 2000)),
        ( "Tire Patching and Repair", choice([87, 127, 88, 128]), randint(250, 2000)),
        ( "Tire Balancing", choice([87, 127, 88, 128]), randint(250, 2000)),
        ( "Tire Rotation", choice([87, 127, 88, 128]), randint(250, 500)),
        ( "Wheel Alignment", choice([87, 127, 88, 128]), randint(500, 1000)),
        ( "Fuel Filter Replacement", choice([89, 129, 90, 130]), randint(250, 750)),
        ( "Fuel Injector Cleaning and Replacement", choice([89, 129, 90, 130]), randint(500, 1250)),
        ( "Fuel Line Repair and Replacement", choice([89, 129, 90, 130]), randint(750, 1500)),
        ( "Fuel Tank Cleaning and Repair", choice([89, 129, 90, 130]), randint(500, 1250)),
        ( "Fuel Pressure Regulator Replacement", choice([89, 129, 90, 130]), randint(500, 1000)),
        ( "Fuel Pump Replacement", choice([89, 129, 90, 130]), randint(500, 1500)),
        ( "Full Vehicle Repainting", choice([91, 131, 92, 132]), randint(3000, 5000)),
        ( "Panel Spot Painting", choice([91, 131, 92, 132]), randint(1500, 2500)),
        ( "Scratch and Chip Touch-up", choice([91, 131, 92, 132]), randint(500, 1000)),
        ( "Clear Coat Application", choice([91, 131, 92, 132]), randint(500, 1000)),
        ( "Custom Paint Jobs", choice([91, 131, 92, 132]), randint(2500, 7500)),
        ( "Exterior Hand Wash", choice([93, 133, 94, 134]), randint(250, 500)),
        ( "Interior Vacuuming and Cleaning", choice([93, 133, 94, 134]), randint(250, 500)),
        ( "Window Cleaning", choice([93, 133, 94, 134]), randint(150, 350)),
        ( "Waxing and Polishing", choice([93, 133, 94, 134]), randint(500, 1000)),
        ( "Tire and Wheel Cleaning", choice([93, 133, 94, 134]), randint(150, 350)),
        ( "Undercarriage Cleaning", choice([93, 133, 94, 134]), randint(250, 500)),
        ( "Engine Bay Cleaning", choice([95, 133, 94, 134]), randint(250, 500)),
        ( "Alternator Replacement", choice([61, 101, 62, 102]), randint(500, 2000)),
        ( "Starter Motor Replacement", choice([61, 101, 62, 102]), randint(500, 1500)),
        ( "Radiator Replacement", choice([61, 101, 62, 102]), randint(500, 1500)),
        ( "Water Pump Replacement", choice([61, 101, 62, 102]), randint(1000, 3000)),
        ( "Fuel Pump Replacement", choice([61, 101, 62, 102]), randint(1000, 3000)),
        ( "Exhaust System Repair", choice([61, 101, 62, 102]), randint(1000, 10000)),
        ( "Clutch Replacement", choice([63, 103, 64, 104]), randint(3000, 8000)),
        ( "Shock Absorber Replacement", choice([63, 103, 64, 104]), randint(1500, 4000)),
        ( "Brake Pad Replacement", choice([63, 103, 64, 104]), randint(1000, 3000)),
        ( "Brake Disc Replacement", choice([63, 103, 64, 104]), randint(2000, 5000)),
        ( "Suspension Repair", choice([63, 103, 64, 104]), randint(1500, 5000)),
        ( "Electronics Diagnostic", choice([65, 105, 66, 106]), randint(500, 1500)),
        ( "Wiring Repair and Replacement", choice([65, 105, 66, 106]), randint(1000, 3000)),
        ( "Alternator Testing and Repair", choice([65, 105, 66, 106]), randint(1000, 3000)),
        ( "Starter Motor Testing and Repair", choice([65, 105, 66, 106]), randint(1000, 3000)),
        ( "Fuse and Relay Replacement", choice([65, 105, 66, 106]), randint(500, 1000)),
        ( "Ignition System Repair", choice([65, 105, 66, 106]), randint(1500, 4000)),
        ( "Lighting System Repair", choice([65, 105, 66, 106]), randint(500, 2000)),
        ( "Power Window and Door Lock Repair", choice([65, 105, 66, 106]), randint(1000, 3000)),
        ( "Starter Motor Testing and Repair", choice([67, 107, 68, 108]), randint(1000, 3000)),
        ( "Voltage Regulator Replacement", choice([67, 107, 68, 108]), randint(1000, 2500)),
        ( "Charging System Diagnostics and Repair", choice([67, 107, 68, 108]), randint(1000, 3000)),
        ( "Electrical System Voltage Testing", choice([67, 107, 68, 108]), randint(500, 1000)),
        ( "Charging System Diagnostics and Repair", choice([67, 107, 68, 108]), randint(1000, 3000)),
        ( "Power Inverter Installation and Repair", choice([67, 107, 68, 108]), randint(1500, 4000)),
        ( "Generator Testing and Repair", choice([67, 107, 68, 108]), randint(1000, 3000)),
        ( "Power Steering System Repair", choice([67, 107, 68, 108]), randint(1500, 4000)),
        ( "Electric Fan and Cooling System Repair", choice([67, 107, 68, 108]), randint(1000, 2500)),
        ( "Battery Testing and Diagnosis", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Replacement", choice([69, 109, 70, 110]), randint(1000, 1500)),
        ( "Battery Terminal Cleaning and Maintenance", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Cable Replacement", choice([69, 109, 70, 110]), randint(1000, 2000)),
        ( "Battery Load Testing", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Charging System Inspection", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Voltage Testing and Calibration", choice([69, 109, 70, 110]), randint(500, 1000)),
        ( "Battery Disposal and Recycling", choice([69, 109, 70, 110]), randint(50, 1000)),
        ( "Tire Patching and Repair", choice([71, 111, 72, 112]), randint(500, 4000)),
        ( "Tire Balancing", choice([71, 111, 72, 112]), randint(500, 4000)),
        ( "Tire Rotation", choice([71, 111, 72, 112]), randint(500, 1000)),
        ( "Tire Alignment Check", choice([71, 111, 72, 112]), randint(500, 4000)),
        ( "Spring Replacement", choice([73, 113, 74, 114]), randint(1500, 5000)),
        ( "Suspension Spring Adjustment", choice([73, 113, 74, 114]), randint(1500, 4000)),
        ( "Leaf Spring Repair", choice([73, 113, 74, 114]), randint(2000, 5000)),
        ( "Coil Spring Inspection and Maintenance",choice([73, 113, 74, 114]),  randint(1000,2500)),
        ( "Spring Bushing Replacement", choice([73, 113, 74, 114]), randint(1000, 3000)),
        ( "Electrical Wiring Repair", choice([75, 115, 76, 116]), randint(1000, 3000)),
        ( "Wire Splicing and Soldering", choice([75, 115, 76, 116]), randint(1000, 3000)),
        ( "Connector Replacement", choice([75, 115, 76, 116]), randint(1000, 3000)),
        ( "Cable Harness Repair", choice([75, 115, 76, 116]), randint(1500, 4000)),
        ( "Circuit Board Soldering", choice([75, 115, 76, 116]), randint(1000, 3000)),
        ( "Body Panel Welding and Repair", choice([77, 117, 78, 118]), randint(1000, 4000)),
        ( "Frame Straightening and Welding", choice([77, 117, 78, 118]), randint(2000, 5000)),
        ( "Exhaust System Welding and Repair", choice([77, 117, 78, 118]), randint(1000, 3000)),
        ( "Chassis Welding and Reinforcement", choice([77, 117, 78, 118]), randint(1500, 4000)),
        ( "Rust and Corrosion Repair", choice([77, 117, 78, 118]), randint(1000, 3000)),
        ( "Dent Repair and Removal", choice([79, 119, 80, 120]), randint(1000, 3000)),
        ( "Rust and Corrosion Repair", choice([79, 119, 80, 120]), randint(1000, 3000)),
        ( "Scratch and Scuff Removal", choice([79, 119, 80, 120]), randint(500, 1500)),
        ( "Panel Replacement and Welding", choice([79, 119, 80, 120]), randint(2000, 5000)),
        ( "Sheet Metal Fabrication", choice([79, 119, 80, 120]), randint(1500, 4000)),
        ( "Bodywork Repair", choice([79, 119, 80, 120]), randint(1000, 5000)),
        ( "Sheet metal tasks", choice([79, 119, 80, 120]), randint(1000, 5000)),
        ( "Structural Frame Reinforcement", choice([81, 121, 82, 122]), randint(1500, 4000)),
        ( "Chassis Strengthening", choice([81, 121, 82, 122]), randint(1500, 4000)),
        ( "Roll Cage Installation", choice([81, 121, 82, 122]), randint(2000, 5000)),
        ( "Frame Welding and Repair", choice([81, 121, 82, 122]), randint(1500, 4000)),
        ( "Structural Member Replacement", choice([81, 121, 82, 122]), randint(1500, 4000)),
        ( "Seat Repair and Upholstery", choice([83, 123, 84, 124]), randint(1500, 4000)),
        ( "Dashboard and Console Refurbishment", choice([83, 123, 84, 124]), randint(1500, 3000)),
        ( "Carpet Replacement and Cleaning", choice([83, 123, 84, 124]), randint(1000, 2000)),
        ( "Headliner Repair and Replacement", choice([83, 123, 84, 124]), randint(1000, 2500)),
        ( "Interior Detailing and Restoration", choice([83, 123, 84, 124]), randint(1000, 3000)),
        ( "Interior Wooden Trim Repair", choice([85, 125, 86, 126]), randint(1000, 3000)),
        ( "Dashboard Wood Veneer Repair", choice([85, 125, 86, 126]), randint(1500, 4000)),
        ( "Wooden Steering Wheel Refinishing", choice([85, 125, 86, 126]), randint(1500, 4000)),
        ( "Wooden Panel Replacement", choice([85, 125, 86, 126]), randint(2000, 5000)),
        ( "Wooden Gear Shift Knob Restoration", choice([85, 125, 86, 126]), randint(1000, 2500)),
        ( "Tire Replacement", choice([87, 127, 88, 128]), randint(500, 4000)),
        ( "Tire Patching and Repair", choice([87, 127, 88, 128]), randint(500, 4000)),
        ( "Tire Balancing", choice([87, 127, 88, 128]), randint(500, 4000)),
        ( "Tire Rotation", choice([87, 127, 88, 128]), randint(500, 1000)),
        ( "Wheel Alignment", choice([87, 127, 88, 128]), randint(1000, 2000)),
        ( "Fuel Filter Replacement", choice([89, 129, 90, 130]), randint(500, 1500)),
        ( "Fuel Injector Cleaning and Replacement", choice([89, 129, 90, 130]), randint(1000, 2500)),
        ( "Fuel Line Repair and Replacement", choice([89, 129, 90, 130]), randint(1500, 3000)),
        ( "Fuel Tank Cleaning and Repair", choice([89, 129, 90, 130]), randint(1000, 2500)),
        ( "Fuel Pressure Regulator Replacement", choice([89, 129, 90, 130]), randint(1000, 2000)),
        ( "Fuel Pump Replacement", choice([89, 129, 90, 130]), randint(1000, 3000)),
        ( "Full Vehicle Repainting", choice([91, 131, 92, 132]), randint(6000, 10000)),
        ( "Panel Spot Painting", choice([91, 131, 92, 132]), randint(3000, 5000)),
        ( "Scratch and Chip Touch-up", choice([91, 131, 92, 132]), randint(1000, 2000)),
        ( "Clear Coat Application", choice([91, 131, 92, 132]), randint(1000, 2000)),
        ( "Custom Paint Jobs", choice([91, 131, 92, 132]), randint(5000, 15000)),
        ( "Exterior Hand Wash", choice([93, 133, 94, 134]), randint(500, 1000)),
        ( "Interior Vacuuming and Cleaning", choice([93, 133, 94, 134]), randint(500, 1000)),
        ( "Window Cleaning", choice([93, 133, 94, 134]), randint(300, 700)),
        ( "Waxing and Polishing", choice([93, 133, 94, 134]), randint(1000, 2000)),
        ( "Tire and Wheel Cleaning", choice([93, 133, 94, 134]), randint(300, 700)),
        ( "Undercarriage Cleaning", choice([93, 133, 94, 134]), randint(500, 1000)),
        ( "Engine Bay Cleaning", choice([95, 133, 94, 134]), randint(500, 1000))
    )
    return choice(RepairType)

DATE_ALPHA = date(2022,9,1)
DATE_OMEGA = date.today()

def GenerateDates(fr: date, to: date):
    route = fr

    while route <= to:
        yield route
        route += timedelta(days=1)

dates = GenerateDates(DATE_ALPHA, DATE_OMEGA)

id = 0
data = []
try:
    while True:
        for date in GenerateDates(DATE_ALPHA, DATE_OMEGA):
            for _ in range(1, 10):
                IdVehicle = randint(1, 44)
                id += 1
                data.append((id, date, IdVehicle, *TypeEmCost()))
                if id >= 10000:
                    raise
except: pass



print(tabulate(data))

#    Id, RepairDate, IdVehicle, RepairType,  IdEmployee, Cost

CURSOR.executemany("INSERT INTO Repair VALUES(?,?,?,?,?,?)", data)
CONNECTION.commit()





