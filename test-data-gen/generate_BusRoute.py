from datetime import datetime, timedelta, time
import pandas as pd
import os
import sqlite3
from sqlite3 import Connection, Cursor

from random import randint, choice, choices

from tabulate import tabulate

RES_DIR = os.path.dirname(__file__)

CONNECTION = sqlite3.connect(os.path.join(RES_DIR, 'mydata.db'))
CURSOR = CONNECTION.cursor()

DATETIME_ALPHA = datetime(2022,9,1,6,0)
time_interval = timedelta(minutes=60)
TIME_STOP = timedelta(hours=23)
DATE_OMEGA = datetime.today()
#route_date = pd.date_range(DATETIME_ALPHA, DATE_OMEGA, freq="60min")


def GenerateDates(fr: datetime, to: datetime):
    route = fr

    while route <= to:
        for hour in (7, 13, 20):
            yield datetime.combine(route.date(), time.min.replace(hour=hour))
        route += timedelta(days=1)

dates = GenerateDates(DATETIME_ALPHA, DATE_OMEGA)



routes = (
    (1,'TZSM - Likarnya', 1, 12.6),
    (2,'Yablunevyj - Likarnya', 2, 14.4),
    (3,'Pischana - Yablunevyj', 4, 13.4),
    (4,'Pischana - Avtobaza', 5, 9.6),
    (5,'Pischana - Likarnya', 6, 4.6),
    (6,'Pischana - Lukavitza', 7, 6.8),
    (7,'Pischana - Blakytivka', 8, 9.8),
    (8,'Pischana - 8-go lystopada', 9, 9.8),
    (9,'Pischana - Polok', 10, 10.8),
    (10,'Pischana - Yarovivska', 11, 12.0),
    (11,'Pischana - Soborna', 12, 6.4),
    (12,'Pischana - Zhevagi', 13, 11.2),
    (13,'Avtostantziya - Plyazh', 15, 26.0),
    (14,'Avtostantziya - Ukrainka', 16, 22.0),
    (15,'Avtostantziya - 1-sche Travnya', 17, 13.8),
    (16,'Avtostantziya - Kopachiv', 18, 30.0),
    (17,'Avtostantziya - Derevyana', 19, 15.0),
    (18,'Avtostantziya - Nescheriv', 20, 13.4),
    (19,'Avtostantziya - Tatzenki', 21, 16.6),
    (20,'Avtostantziya - Scherbanivka', 22, 22.0),
)

id = 0
data = []
for route in routes:
    for date in GenerateDates(DATETIME_ALPHA, DATE_OMEGA):
        id += 1
        data.append((id, date, *route))

print(tabulate(data))

CURSOR.executemany("INSERT INTO BusRoute VALUES(?,?,?,?,?,?)", data)
CONNECTION.commit()



if __name__== "__main__":
    print('DATETIME_ALPHA=',DATETIME_ALPHA)
    # print(*list(GenerateDates(DATETIME_ALPHA,DATE_OMEGA)), sep = '\n')
    for d in GenerateDates(DATETIME_ALPHA,DATE_OMEGA): print(d)
    print('DATETIME_ALPHA=',DATETIME_ALPHA)
