from datetime import date, timedelta
import os
import sqlite3
from sqlite3 import Connection, Cursor

from random import randint, choice, choices
from itertools import zip_longest
from tabulate import tabulate

RES_DIR = os.path.dirname(__file__)

CONNECTION = sqlite3.connect(os.path.join(RES_DIR, 'mydata.db'))
CURSOR = CONNECTION.cursor()

def Name():
    return choice([
    'Antonina', 'Nina', 'Borys', 'Oleh','Viktor', 'Pavlo', 'Halyna', 'Raisa', 'Svitlana', 'Dmitry', 'Tamara', 'Elvira', 'Larysa', 'Yaroslav',
    'Uliana', 'Yevheniia', 'Fedir', 'Zhanna', 'Khrystyna', 'Zakhar', 'Mykyta', 'Iryna', 'Zinaida', 'Yosyp', 'Tymofii', 'Kateryna', 'Yury', 'Margaryta'
    ])


def Surname():
    return choice([
    'Akimenko', 'Aleksandrenko', 'Alekseenko', 'Andrievskiy', 'Andriyashev', 'Ardashev', 'Artemenko', 'Artemenko', 'Babarika','Babaryka','Babich','Barskiy',
    'Batyuk','Beleckiy','Belogradskiy','Belyavskiy','Berezhinskiy','Beskrovnyy','Bilozir','Boguslavskiy','Boyko','Bondarenko','Bondarchuk','Borzenko','Borisenko',
    'Boroday','Bortnik','Brandys','Brovchenko','Bubenko','Bugaenko','Budennyy','Burkovskiy','Buryy','Buteyko','Butenko','Butko','Vakulenko','Vasilevskiy','Vasil\'enko',
	'Vasilyuk','Vdovenko','Vdovichenko','Velichko','Viktorenko','Vilenskiy','Vinnichenko','Vinnichuk','Vlaschenko','Vovk','Voytenko','Volynec','Garmash','Gvozdeckiy',
    'Gerasimenko','Geraschenko','Golovko','Golub','Golubovskiy','Goncharenko','Goncharuk','Gorbenko','Gorenko','Grabchak','Grib', 'Grigorchuk','Grischuk','Gubenko','Gubskiy',
	'Gutnik','Davydovskiy','Daragan', 'Derevyanko','Dzyuba','Dzyubenko','Dmitrenko','Dmitrienko','Dolgopol','Donec','Doroshenko','Drizhenko','Drobyazko','Dubenko','Dubovik','Dudarenko','Dudko',
	'Evseenko','Evtushenko','Emel\'yanenko','Eremenko','Efimenko','Zheleznyak','Zhinkin','Zhiteckiy','Zhuchenko','Zavgorodniy','Zav\'yalova', 'Zagaykevich','Zagdanskiy','Zaderackiy','Zadneprovskiy',
    'Zadorozhnyy','Zakrevskiy','Zaliznyak','Zaporozhan','Zasyad', 'Zatyrkevich','Zaharenko','Zaharchenko','Zgurskiy','Zelenko','Zinchenko','Zozulya','Ivanenko','Ivanchenko','Ischenko','Kavun',
    'Kalenichenko','Kalinichenko','Kalinovskiy','Kalligraf','Kanevskiy','Karpenko','Kirienko','Kirilenko','Kirichenko','Kistyakovskiy','Kiyashko','Klimenko','Kniga','Knyazevich','Kovalenko','Koval',
	'Kovan', 'Kovpak','Kozakov','Kozickiy','Kozlovskie','Kozlovskiy','Kolesnichenko','Kolin','Kolomiec','Komarovskiy','Kondratenko','Kondratyuk','Kornienko','Kosenko','Kostyuk','Kotlyar','Kotlyarevskiy','Kochubey','Koshevoy','Kravec','Kravchenko','Kravchuk','Krasko','Kras',
	'Krachkovskiy','Krivoruchko','Kudrya','Kulish','Kurilenko','Kucherenko','Kucheryavyy','Kuchma','Kushnir','Lavrenko','Lanovoy','Lebedenko','Levin','Levchenko','Leschenko','Lizogub','Linnik','Lisenko','Lisovec','Litvinenko','Lukashenko',	'Lucenko','Lysenko','Lyubchenko',
    'May','Maysak','Makuha','Malanyuk','Malkovich','Mamaev','Markevich','Markitan','Martynyuk','Marchenko','Maslovskiy','Maslyukov','Matveenko','Matvienko','Matyushenko','Mahnenko','Mel\'nik',
	'Merezhko','Merezhkovskiy','Miklashevskie','Mikluha','Mirgorodskiy','Mihaylenko','Mischenko','Movchan','Mozgovoy','Moiseenko','Moroz','Morohovec','Naumenko', 'Nesterenko','Nikolaenko','Ovcharenko','Omel\'chenko',
	'Onischenko','Onoprienko','Opanasenko','Osipenko','Ostapenko','Ostapchuk','Ostrovskiy','Pavlenko','Palamarchuk','Panasyuk','Paskevich','Paschenko','Perepadenko','Petrenko','Petrichenko','Pilipenko','Pisarenko',
    'Pokotilo','Pokrovskiy','Poleschuk','Polischuk','Polonskiy','Poltorackiy','Polyanskiy','Ponomarenko','Popovich','Poroshenko','Pridius','Priyma','Prokopenko','Prudius','Pustovoytenko','Pushkar',
    'Reznik','Roy','Romanenko','Romanyuk','Ruban','Rudenko','Rybalko','Ryabovol','Ryahovskiy','Savenko','Savchuk','Sagaydachnyy','Salko','Samiylenko','Svinarenko','Semashko','Semenovich','Serbin','Serbinenko','Sergienko','Serdyuk','Serdyukov','Sereda','Sikorskiy',
	'Tabachnik','Tarasenko','Tereschenko','Ternovskiy','Timoschuk','Timchenko','Tischenko','Tkachenko','Troschinskiy','Tyutyunnik','Udovenko','Udovichenko','Umanskiy','Fedoriv','Filipenko','Filonenko','Fomenko','Harchenko','Homenko','Hruschev','Hruschov',
    'Cvigun','Cybulenko','Chayka','Chalyy','Chvertko','Chervonenko','Cherkasskiy','Chernovol','Chernyy','Chernyahovskiy','Chizhevskiy','Chubinskiy','Shevchenko','Shevchuk','Shelest','Shischenko','Shklyar','Shostak','Shpak','Shukalov',
	'Shumskiy','Scherbak','Scherban','Schurovskiy','Yurchenko','Yuschenko','Yakovina','Yakushenko','Yanenko', 'Hmel\'nickiy','Yaroslavskiy','Yaroshenko'
    ])


def Profession():
    return choice([
        'Locksmith','Aggregator', 'Minder', 'Machinist', 'Electrician', 'Tire fitter', 'Bodybuilder', 'Tinsmith', 'Carpenter', 'Washer', 'Carburetor', 'Fuel valver',
        'Vulcanizer', 'Painter', 'Thermist', 'Coppersmith', 'Accumulator', 'Welder', 'Dispatcher', 'Driver'
        ])



def IdTeam():
    return choice([
        'Administration', 'Drivers', 'Aggregate', 'Locksmith', 'Electrical', 'Power system devices', 'Battery',
        'Vulcanizing', 'Forging&Spring',  'Copper', 'Welding', 'Tin', 'Reinforcing', 'Wallpaper', 'Woodworking',
        'Tire', 'Fuel', 'Paint', 'Car wash', 'Dispatcher'])


def ReportTo():
    pass



profession = ['Aggregator', 'Locksmith', 'Electrician', 'Carburetor', 'Accumulator', 'Vulcanizer', 'Machinist','Coppersmith', 
              'Welder', 'Tinsmith', 'Fuel valver adjuster', 'Upholsterer', 'Carpenter','Tire fitter', 'Thermist', 'Painter', 
              'Washer', 'Dispatcher', 'Driver']

profession2 = ['Aggregator','Aggregator', 'Locksmith','Locksmith', 'Electrician','Electrician', 'Carburetor','Carburetor', 
               'Accumulator','Accumulator', 'Vulcanizer','Vulcanizer', 'Machinist','Machinist','Coppersmith', 'Coppersmith',
              'Welder','Welder', 'Tinsmith','Tinsmith', 'Fuel valver adjuster','Fuel valver adjuster', 'Upholsterer','Upholsterer',
                'Carpenter','Carpenter','Tire fitter','Tire fitter', 'Thermist','Thermist', 'Painter', 'Painter',
              'Washer','Washer', 'Dispatcher','Dispatcher','Dispatcher', 'Driver','Driver','Driver']



HeadLeads = {2:'HeadAggregator', 3:'HeadLocksmith', 4:'HeadElectrician', 5:'HeadCarburetor', 6:'HeadAccumulator', 7:'HeadVulcanizer', 
             8:'HeadMachinist', 9:'HeadCoppersmith', 10:'HeadWelder', 11:'HeadTinsmith', 12:'HeadFuel valver adjuster', 
             13:'HeadUpholstery', 14:'HeadCarpenter', 15:'HeadTire fitter', 16:'HeadThermist', 17:'HeadPainter', 18:'HeadWasher', 
             19:'HeadDispatcher', 20:'HeadHeadriver'}

workshops = {2: [2, 3], 3: [4, 5], 4: [6, 7], 5: [8, 9], 6: [10, 11],
             7: [12, 13], 8: [14, 15],  9: [16, 17], 10: [18, 19], 11:[20, 21], 12:[22, 23], 13:[24, 25],
             14:[26, 27], 15:[28, 29], 16:[30, 31], 17:[32, 33], 18:[34, 35], 19:[36,37,38], 20:[39,40,41]}


PROF_RAW = {
    # WS    PROF                    TEAMS          REPORTS TO
       2:  ('Aggregate',            [2, 3]      , [[2 ], [21, 22    ], [61, 62    ]]),
       3:  ('Locksmith',            [4, 5]      , [[3 ], [23, 24    ], [63, 64    ]]),
       4:  ('Electrical',           [6, 7]      , [[4 ], [25, 26    ], [65, 66    ]]),
       5:  ('Power system devices', [8, 9]      , [[5 ], [27, 28    ], [67, 68    ]]),
       6:  ('Battery',              [10, 11]    , [[6 ], [29, 30    ], [69, 70    ]]),
       7:  ('Vulcanizing',          [12, 13]    , [[7 ], [31, 32    ], [71, 72    ]]),
       8:  ('Forging&pring',        [14, 15]    , [[8 ], [33, 34    ], [73, 74    ]]),
       9:  ('Copper',               [16, 17]    , [[9 ], [35, 36    ], [75, 76    ]]),
       10: ('Welding',              [18, 19]    , [[10], [37, 38    ], [77, 78    ]]),
       11: ('Tin',                  [20, 21]    , [[11], [39, 40    ], [79, 80    ]]),
       12: ('Reinforcing',          [22, 23]    , [[12], [41, 42    ], [81, 82    ]]),
       13: ('Upholstery',           [24, 25]    , [[13], [43, 44    ], [83, 84    ]]),
       14: ('Woodworking',          [26, 27]    , [[14], [45, 46    ], [85, 86    ]]),
       15: ('Tire',                 [28, 29]    , [[15], [47, 48    ], [87, 88    ]]),
       16: ('Fuel',                 [30, 31]    , [[16], [49, 50    ], [89, 90    ]]),
       17: ('Paint',                [32, 33]    , [[17], [51, 52    ], [91, 92    ]]),
       18: ('Car wash',             [34, 35]    , [[18], [53, 54    ], [93, 94    ]]),
       19: ('Dispatcher',           [36,37,38]  , [[19], [55, 56, 57], [95, 96, 97]]),
       20: ('Driver',               [39,40,41]  , [[20], [58, 59, 60], [98, 99, 100]]),
}


def GenRec(position):
    repto_idx = {
        'Master': 0,
        'TeamLead': 1,
        'Worker': 2
    }[position]
    for wsid, (prof, teams, reports) in PROF_RAW.items():
        reports = reports[repto_idx]
        for team, repto in zip_longest(teams, reports, fillvalue=reports[0]):
            yield prof, position, team, repto
    # i = 0
    # while True:
    #     if i >= len(PROF):
    #         i = 0
    #     wsid, prof, team, repto = PROF[i]
    #     yield prof, 'Master', team, repto
    #     i += 1

    # # 'Profession', 'Position', 'IdTeam', 'ReportTo'
    # # WSID = (ID // 19) + 2
    # prof, teams, repto = PROF[WSID]
    # pos = 'Master'
    # team = teams[ID % len(teams)]
    # return prof, pos, team, repto
Masters = GenRec('Master')
TeamLeads = GenRec('TeamLead')
Workers = GenRec('Worker')

data =  [ (1, Name(), Surname(), 'Economist', 'Director', 1, None) ]
data += [ (ID, Name(), Surname(), profession[ID-2], HeadLeads[ID], 1, 1 ) for ID in range(2, 21)] # HeadLeads
data += [ (ID, Name(), Surname(), *next(Masters)) for ID in range(21, 61)] # Masters
data += [ (ID, Name(), Surname(), *next(TeamLeads)) for ID in range(61, 101)] # TeamLeads
data += [ (ID, Name(), Surname(), *next(Workers)) for ID in range(101, 141)] # Workers
data += [ (ID, Name(), Surname(), 'Driver', 'Worker', 39, 98) for ID in range(141, 179)] #BusDrivers
data += [ (ID, Name(), Surname(), 'Driver', 'Worker', 40, 99) for ID in range(179, 193)] #CargoDrivers
data += [ (ID, Name(), Surname(), 'Driver', 'Worker', 41, 100) for ID in range(193, 213)] #TaxiDrivers

print(tabulate(data, tablefmt='plain', headers=['Id', 'Name', 'Surname', 'Profession', 'Position', 'IdTeam', 'ReportTo']))

CURSOR.executemany("INSERT INTO Employees VALUES(?,?,?,?,?,?,?)", data)
CONNECTION.commit()

