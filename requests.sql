/*Request1*/ --Obtain data about the company's vehicle fleet
SELECT *
FROM Vehicle;

--Request2a Get a list of drivers within the company
SELECT d.IdEmployees, e.Name, e.Surname, e."Position", v.Id AS IdVehicle, v.Brand
FROM Distribution AS d
JOIN Employees AS e ON d.IdEmployees = e.Id
JOIN Vehicle AS v ON d.IdVehicle = v.Id
WHERE e.Profession LIKE "Driver"
ORDER BY v.Id;

--Request2b Get a total number of drivers within the company
SELECT COUNT(Id) AS QntDrivers, Profession
FROM Employees
WHERE Profession LIKE 'Driver' AND ("Position" LIKE 'Worker' OR "Position" LIKE 'TeamLead');

--Request2c Get a total number of drivers for a specific vehicle
SELECT v.Id AS IdVehicle, v.RegistrNum, v.Brand, d.IdEmployees, e.Name, e.Surname
FROM Distribution AS d
JOIN Employees AS e ON d.IdEmployees = e.Id
JOIN Vehicle AS v ON d.IdVehicle = v.Id
WHERE e.Profession LIKE "Driver" AND v.Id LIKE 1
ORDER BY v.Id;

--Request3  Obtain distribution of drivers across vehicles
SELECT d.IdVehicle, v.Brand, d.IdEmployees, e.Name, e.Surname
FROM Distribution AS d
JOIN Employees AS e ON d.IdEmployees = e.Id
JOIN Vehicle AS v ON d.IdVehicle = v.Id
ORDER BY d.IdVehicle ;

--Request4  Retrieve data about the distribution of passenger vehicles along routes
SELECT br.Name, v.Id As IdBus, v.Brand
FROM Vehicle AS v JOIN BusRoute AS br ON v.Id = br.IdVehicle
GROUP BY IdRoute;

--Request5  Obtain data about the hierarchy of personnel: workers, teamleads, masters and workshop heads
SELECT A.Id, A.Name, A.Surname, A. Profession, A."Position", A.IdTeam, A.ReportTo, B.Name, B.Surname, B."Position", w.Name AS WSName, B.IdTeam
FROM Employees A, Employees B JOIN Team t ON B.IdTeam = t.ID JOIN Workshop w ON w.Id = t.IdWorkshop
WHERE A.ReportTo = B.Id
ORDER BY A.ReportTo;

--Request6  Get information about the presence of a garage facility overall and for each transport category.
SELECT Garage AS GarageNumber, Id, RegistrNum, Brand, CarType, Category
FROM Vehicle
WHERE Decommissioned IS NULL
ORDER BY Garage;

--Request7  Retrieve data about the distribution of vehicles at the company
SELECT CarType, COUNT(*), Category
FROM Vehicle v
WHERE Decommissioned IS NULL
GROUP BY CarType;

--Request8  Obtain information about the acquisition and disposal of vehicles within a specified period
SELECT Id, RegistrNum, Brand, Commissioned, Decommissioned
FROM Vehicle v
WHERE Commissioned BETWEEN '2009-01-01' AND '2023-01-01'
ORDER BY Decommissioned;

--Request9  Get the roster of subordinates for a specified foreman, supervisor, etc.
SELECT A.Name, A.Surname, A.Profession, A."Position", A.ReportTo, B.Name, B.Surname, B."Position"
FROM Employees A, Employees B
WHERE A.ReportTo = B.Id AND B.Id = 1;

--Request10a Obtain data about tasks performed by a specified specialist within a specified period for a specific vehicle.
SELECT r.RepairDate, e.Name, e.Surname, r.IdVehicle, r.RepairType
FROM Employees AS e JOIN Repair AS r ON e.Id = r.IdEmployee
WHERE e.Id like 73 AND r.IdVehicle LIKE 5 AND r.RepairDate BETWEEN '2022-09-01'AND '2023-08-25';

--Request10b  Obtain data about tasks performed by a specified specialist within a specified period overall
SELECT r.RepairDate, e.Name, e.Surname, r.IdVehicle, r.RepairType
FROM Employees AS e JOIN Repair AS r ON e.Id = r.IdEmployee
WHERE e.Id LIKE 73 AND r.RepairDate BETWEEN '2022-09-01' AND '2023-08-25';

--Request11a   Retrieve information about the mileage of a certain vehicle category on a specified day, month, and year.
SELECT v.Id, v.CarType, SUM(br.Mileage)
FROM BusRoute As br JOIN Vehicle AS v ON v.Id = br.IdVehicle
WHERE v.CarType LIKE 'Bus' AND br.RouteDate BETWEEN '2022-09-01' AND '2023-08-13';

--Request11b  Retrieve information about the mileage of specific vehicle on a specified day, month, and year.
SELECT v.Id, v.RegistrNum, v.CarType, SUM(cr.Mileage)
FROM CargoRoute AS cr JOIN Vehicle AS v ON v.Id = cr.IdVehicle
WHERE cr.IdVehicle LIKE 33 AND cr.RouteDate BETWEEN '2022-09-01' AND '2023-08-13';

--Request 12a  Get data about the number of repairs and their costs for a specific vehicle category
SELECT Count(r.Id) AS QntRepairs, SUM(r.Cost)
FROM Repair AS r JOIN Vehicle AS v  ON v.Id = r.IdVehicle
WHERE v.Category LIKE "Passenger" AND r.RepairDate BETWEEN '2022-09-01'AND '2023-08-25';

--Request12b   Get data about the number of repairs and their costs for a specific vehicle brand within a given period.
SELECT  Count(r.Id) AS QntRepairs, v.Brand, SUM(r.Cost)
FROM Repair AS r JOIN Vehicle AS v  ON v.Id = r.IdVehicle
WHERE v.Brand LIKE "Scoda%" AND r.RepairDate BETWEEN '2022-09-01'AND '2023-08-25';

--Request12c  Get data about the number of repairs and their costs for a specified vehicle within a given period.
SELECT  v.RegistrNum, v.Brand, Count(r.Id) AS QntRepairs, SUM(r.Cost)
FROM Repair AS r JOIN Vehicle AS v  ON v.Id = r.IdVehicle
WHERE v.Id LIKE 2 AND r.RepairDate BETWEEN '2023-02-01'AND '2023-03-01';

--Request13   Obtain information about cargo transportation carried out by a specified vehicle during a defined period.
SELECT cr.RouteDate, v.Id, v.RegistrNum, v.Brand, e.Name, e.Surname, cr.CargoType,cr.Destination
FROM Vehicle v
JOIN CargoRoute cr ON v.Id = cr.IdVehicle
JOIN Employees e ON cr.IdDriver = e.Id
WHERE v.Id = 23 AND cr.RouteDate BETWEEN '2022-09-01' AND '2023-08-25';

--Request14a Get data about the number of used components and assemblies for repair within a specified period, categorized by vehicle brand
FROM Repair r JOIN Vehicle v ON r.IdVehicle = v.Id
WHERE v.Brand LIKE 'Ruta 37' AND r.RepairDate BETWEEN '2022-09-01' AND '2023-08-25'
GROUP BY r.RepairType;

--Request14b  Get data about the number of used components and assemblies for repair within a specified period, categorized by vehicle type
SELECT COUNT(r.IdVehicle) as Quantity, r.RepairType, v.Category, v.CarType
FROM Repair r JOIN Vehicle v ON r.IdVehicle = v.Id
WHERE v.Category LIKE 'Passenger' AND v.CarType LIKE 'Bus' AND r.RepairDate BETWEEN '2022-09-01' AND '2023-08-25'
GROUP BY r.RepairType;

--Request14c  Get data about the number of used components and assemblies for repair within a specified period for specific vehicle
SELECT r.IdVehicle, Count(v.RegistrNum) AS QntRepairType, v.Brand, r.RepairType
FROM Repair r JOIN Vehicle v ON r.IdVehicle = v.Id
WHERE r.IdVehicle LIKE 23 AND r.RepairDate BETWEEN '2023-02-01' AND '2023-03-01'
GROUP BY r.RepairType;