PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS Vehicle (
    Id integer PRIMARY KEY,
    RegistrNum varchar NOT NULL,
    Brand varchar NOT NULL,
    Launched integer,
    Commissioned DATE,
    Decommissioned DATE,
    Category varchar,
    CarType varchar,
    Capacity integer,
    Garage integer
);

CREATE TABLE IF NOT EXISTS BusRoute (
    Id integer PRIMARY KEY,
    [RouteDate] DATETIME,
    IdRoute integer,
    [Name] varchar NOT NULL,
    IdVehicle integer,
    Mileage float,
    FOREIGN KEY(IdVehicle) REFERENCES Vehicle(Id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS CargoRoute (
    Id integer PRIMARY KEY,
    [RouteDate] DATE,
    IdVehicle integer,
    IdDriver integer,
    CargoType varchar,
    Destination varchar,
    Mileage integer,
    FOREIGN KEY(IdVehicle) REFERENCES Vehicle(Id) ON DELETE CASCADE,
    FOREIGN KEY(IdDriver) REFERENCES Employees(Id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Employees (
    Id integer PRIMARY KEY,
    [Name] varchar NOT NULL,
    Surname varchar NOT NULL,
    Profession varchar,
    Position varchar,
    IdTeam integer,
    ReportTo varchar,
    FOREIGN KEY(IdTeam) REFERENCES Team(Id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS [Distribution] (
    Id integer PRIMARY KEY,
    IdEmployee varchar NOT NULL,
    IdVehicle integer,
    FOREIGN KEY(IdEmployee) REFERENCES Employees(Id) ON DELETE CASCADE,
    FOREIGN KEY(IdVehicle) REFERENCES Vehicle(Id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Repair (
    Id integer PRIMARY KEY,
    RepairDate DATE,
    IdVehicle integer,
    RepairType varchar,
    IdEmployee integer,
    Cost float,
    FOREIGN KEY(IdEmployee) REFERENCES Employees(Id) ON DELETE CASCADE,
    FOREIGN KEY(IdVehicle) REFERENCES Vehicle(Id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Workshop (
    Id integer PRIMARY KEY,
    [Name] varchar NOT NULL
);

CREATE TABLE IF NOT EXISTS Team (
    Id integer PRIMARY KEY,
    IdWorkshop integer,
    FOREIGN KEY(IdWorkshop) REFERENCES Workshop(Id) ON DELETE CASCADE
);



